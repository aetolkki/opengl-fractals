Libraries required:
glfw 
glad 
stb_image

Headers for glfw and glad in /usr/include,
other files in /src in the project folder. 
 

In linux, build from command line:
g++ -o fractals-bin fractals-v12-skybox.cpp matrix_calc-v9.cpp windowcontrols.cpp src/glad.c -lglfw -ldl -std=c++17 -Wall

Run:
./fractals-bin

