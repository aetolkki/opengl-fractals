#include "windowcontrols.h"

MyWindow::MyWindow(float x, float y, float zoom, float aspect, float distance) 
	: m_x{ x }, m_y{ y }, m_zoom{ zoom }, m_aspect{ aspect }, m_distance{ distance }
{
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
#ifdef __APPLE__
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
#endif
	mWindow = glfwCreateWindow(800, 600, "Fractal Heightmaps", NULL, NULL);
	if (mWindow == NULL)
	{
		std::cout << "window failed" << '\n';
		glfwTerminate();	
	} 
	glfwMakeContextCurrent(mWindow);
	glfwSetWindowUserPointer(mWindow, this);
	glfwSetKeyCallback(mWindow, onKey);
	glfwSetFramebufferSizeCallback(mWindow, onFramebufferResize);
	glfwSetCursorPosCallback(mWindow, onCursorPos);
	glfwSetMouseButtonCallback(mWindow, onMouseButton);
	glfwSetScrollCallback(mWindow, OnScroll);

	// glad: load all OpenGL function pointers
	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
	{
		std::cout << "Failed to initialize GLAD" << std::endl;
		//return -1;
	}
	mMonitor = glfwGetPrimaryMonitor();
	mVidmode = glfwGetVideoMode(mMonitor);


	keys_mapped.push_back({ GLFW_KEY_W, static_cast<void(*)(MyWindow&)>([](MyWindow& mw) { mw.m_x -= mw.m_dt * mw.m_zoom * mw.m_zrota[4]; mw.m_y += mw.m_dt * mw.m_zoom * mw.m_zrota[5]; }) } );
	keys_mapped.push_back({GLFW_KEY_A, static_cast<void(*)(MyWindow&)>([](MyWindow& mw) { mw.m_x -= mw.m_dt * mw.m_zoom * mw.m_zrota[0]; mw.m_y += mw.m_dt * mw.m_zoom * mw.m_zrota[1]; }) });
	keys_mapped.push_back({ GLFW_KEY_S, static_cast<void(*)(MyWindow&)>([](MyWindow& mw) { mw.m_x += mw.m_dt * mw.m_zoom * mw.m_zrota[4]; mw.m_y -= mw.m_dt * mw.m_zoom * mw.m_zrota[5]; }) });
	keys_mapped.push_back({ GLFW_KEY_D, static_cast<void(*)(MyWindow&)>([](MyWindow& mw) { mw.m_x += mw.m_dt * mw.m_zoom * mw.m_zrota[0]; mw.m_y -= mw.m_dt * mw.m_zoom * mw.m_zrota[1]; }) });
	keys_mapped.push_back({ GLFW_KEY_Z, static_cast<void(*)(MyWindow&)>([](MyWindow& mw) { mw.m_zoom *= 0.99f; }) });
	keys_mapped.push_back({ GLFW_KEY_X, static_cast<void(*)(MyWindow&)>([](MyWindow& mw) { mw.m_zoom /= 0.99f; }) });
	keys_mapped.push_back({ GLFW_KEY_UP, static_cast<void(*)(MyWindow&)>([](MyWindow& mw) { mw.m_distance -= 0.01f; }) });
	keys_mapped.push_back({ GLFW_KEY_DOWN, static_cast<void(*)(MyWindow&)>([](MyWindow& mw) { mw.m_distance += 0.01f; }) });
	//keys_mapped.push_back({ GLFW_KEY_LEFT, static_cast<void(*)(MyWindow&)>([](MyWindow& mw) { mw.m_CameraPhase[0] += 0.01f; }) });
	//keys_mapped.push_back({ GLFW_KEY_RIGHT, static_cast<void(*)(MyWindow&)>([](MyWindow& mw) { mw.m_CameraPhase[0] -= 0.01f; }) });
	keys_mapped.push_back({ GLFW_KEY_LEFT, static_cast<void(*)(MyWindow&)>([](MyWindow& mw) { mw.m_ztime += 0.01f;  }) });  //mw.m_yaw += 0.01f;
	keys_mapped.push_back({ GLFW_KEY_RIGHT, static_cast<void(*)(MyWindow&)>([](MyWindow& mw) { mw.m_ztime -= 0.01f;  }) }); //mw.m_yaw -= 0.01f;
	keys_mapped.push_back({ GLFW_KEY_0, static_cast<void(*)(MyWindow&)>([](MyWindow& mw) { mw.m_mutareset = true; mw.m_pitch = 0.0f; mw.m_yaw = -(float)M_PI / 2.0f; }) });
	keys_mapped.push_back({ GLFW_KEY_C, static_cast<void(*)(MyWindow&)>([](MyWindow& mw) { mw.m_colorset = true; }) });
	keys_mapped.push_back({ GLFW_KEY_R, static_cast<void(*)(MyWindow&)>([](MyWindow& mw) { mw.m_mutarandom = true; }) });
	keys_mapped.push_back({ GLFW_KEY_T, static_cast<void(*)(MyWindow&)>([](MyWindow& mw) { mw.m_mutarandom = false; }) });
	keys_mapped.push_back({ GLFW_KEY_ESCAPE, static_cast<void(*)(MyWindow&)>([](MyWindow& mw) { glfwSetWindowShouldClose(mw.mWindow, true); }) });
	//Keys for modeswitch);
	keys_mapped.push_back({ GLFW_KEY_M, static_cast<void(*)(MyWindow&)>([](MyWindow& mw) { mw.m_mode = 1; }) });
	keys_mapped.push_back({ GLFW_KEY_J, static_cast<void(*)(MyWindow&)>([](MyWindow& mw) { mw.m_mode = 2; }) });
	keys_mapped.push_back({ GLFW_KEY_B, static_cast<void(*)(MyWindow&)>([](MyWindow& mw) { mw.m_mode = 3; }) });
	//N Moved to keycallback function	
	//keys_mapped.push_back({ GLFW_KEY_N, static_cast<void(*)(MyWindow&)>([](MyWindow& mw) { mw.m_mode = 4; }) });
	//F/G for fullscreen on/off);
	keys_mapped.push_back({ GLFW_KEY_F, static_cast<void(*)(MyWindow&)>([](MyWindow& mw) { glfwSetWindowMonitor(mw.mWindow, mw.mMonitor, 0, 0, mw.mVidmode->width, mw.mVidmode->height, mw.mVidmode->refreshRate); }) });
	keys_mapped.push_back({ GLFW_KEY_G, static_cast<void(*)(MyWindow&)>([](MyWindow& mw) { glfwSetWindowMonitor(mw.mWindow, NULL, 0, 0, 800, 600, mw.mVidmode->refreshRate); }) });
	//Buttons 1-9 for x, y, zoom presets
	//----------------------------------

}

MyWindow::~MyWindow()
{
	glfwDestroyWindow(mWindow);
	glfwTerminate();
}

void MyWindow::processKeys()
{ 
	for (std::vector<MyWindow::KEYMAP>::iterator it = keys_mapped.begin(); it != keys_mapped.end(); ++it) 
	{
		if (glfwGetKey(mWindow, it->keycode) == GLFW_PRESS) 
		{	
			(it->proc)(*this);
			break;
		}
	} 
}
