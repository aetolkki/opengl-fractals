#ifndef WINDOWCONTROLS_H
#define WINDOWCONTROLS_H

#define _USE_MATH_DEFINES

#include <vector>
#include <iostream>
#include <cmath>
#include <glad/glad.h>
#include <GLFW/glfw3.h>

class MyWindow
{
public:
	MyWindow(float x, float y, float zoom, float aspect, float distance);
	~MyWindow();

	void processKeys();

	void onKey(int key, int scancode, int action, int mods)
	{
		if (key == GLFW_KEY_N && action == GLFW_PRESS)
		{
			(m_mode < 4 || m_mode == 7) ? m_mode = 4 : m_mode += 1;
		}
	}

	// glfw: whenever the window size changed (by OS or user resize) this callback function executes
	void onFramebufferResize(int width, int height) {
		// make sure the viewport matches the new window dimensions; note that width and 
		// height will be significantly larger than specified on retina displays.
		glViewport(0, 0, width, height);
		m_aspect = (float)width / (float)height;
	}

	// mouse events
	void onCursorPos(double xpos, double ypos) {
		m_mousepos[0] = (float)xpos;
		m_mousepos[1] = (float)ypos;

		float mouseSensitivity{ 0.01f };
		if (m_dragging)
		{
			m_mousedrag[0] = m_zoom * mouseSensitivity * (m_initpos[0] - m_mousepos[0]) / 600.0f;
			m_mousedrag[1] = m_zoom * mouseSensitivity * (m_mousepos[1] - m_initpos[1]) / 600.0f;
		}
		else
		{
			m_mousedrag[0] = 0.0f;
			m_mousedrag[1] = 0.0f;
		}

		m_yaw -= m_mousedrag[0];
		m_pitch -= m_mousedrag[1];
		if (m_pitch > 0.9 * (float)M_PI / 2.0f) m_pitch = 0.9 * (float)M_PI / 2.0f;
		else if (m_pitch < -0.9 * (float)M_PI / 2.0f) m_pitch = -0.9 * (float)M_PI / 2.0f;
		if (m_yaw > 0.0f) m_yaw = 0.0f;
		else if (m_yaw < -(float)M_PI) m_yaw = -(float)M_PI;
	}

	void onMouseButton(int button, int action, int mods) {
		//double xpos, ypos;
		if (button == GLFW_MOUSE_BUTTON_LEFT || button == GLFW_MOUSE_BUTTON_RIGHT)
		{
			if (action == GLFW_PRESS)
			{
				m_dragging = true;
				m_initpos[0] = m_mousepos[0];
				m_initpos[1] = m_mousepos[1];
			}
			else if (action == GLFW_RELEASE)
			{
				m_dragging = false;
				m_yaw -= 0.01f * m_mousedrag[0];
				m_pitch -= 0.01f * m_mousedrag[1];
			}
		}
	}


	void OnScroll(double xoffset, double yoffset) {
		if (yoffset > 0) m_fov *= 0.9f;
		else if (yoffset < 0)
		{
			m_fov *= 1.1f;
			if (m_fov > 45.0f) m_fov = 45.0f;
		}
	}

	void updateZrota(float (&zrota)[16]) {
		for (int j{ 0 }; j < 16; ++j) m_zrota[j] = zrota[j];
	}

	void updateYrota(float (&yrota)[16]) {
		for (int k{ 0 }; k < 16; ++k) m_yrota[k] = yrota[k];
	}

	int getMode() {return m_mode;}
	float getX() {return m_x;}
	float getY() {return m_y;}
	float getZoom() {return m_zoom;}
	float getFov() {return m_fov;}
	float getAspect() {return m_aspect;}
	float getYaw() {return m_yaw;}
	float getPitch() {return m_pitch;}
	float getDistance() {return m_distance;}
	float getZtime() {return m_ztime;}
	float* getCameraphase() {return m_CameraPhase;}
	GLFWwindow* getWindow() {return mWindow;}

	bool getMutaReset() {return m_mutareset;}
	bool getMutaRandom() {return m_mutarandom;}
	bool getColorSet() {return m_colorset;}
	void setMutaReset() { m_mutareset = false;}
	void setColorset() { m_colorset = false;}

	void setDt(float dt) { m_dt = dt; } 

	void swapBuffers() {
		glfwSwapBuffers(mWindow);
		glfwPollEvents();
	}

private:
	GLFWwindow* mWindow;
	GLFWmonitor* mMonitor; 
	const GLFWvidmode* mVidmode;
	float m_x{};
	float m_y{};
	float m_zoom{};
	float m_aspect{};
	float m_distance{};

	float m_CameraPhase[2]{};
	float m_zrota[16]{};
	float m_yrota[16]{};

	//Global variables updated in callback functions
	float m_mousepos[2]{};
	float m_mousedrag[2]{};
	float m_initpos[2]{};

	bool m_dragging{false};
	bool m_colorset{false};
	bool m_mutareset{false};
	bool m_mutarandom{false};

	float m_yaw{-(float)M_PI/2.0f};
	float m_pitch{0.0f};
	float m_fov{45.0f};

	float m_dt{};
	int m_mode{1};
	float m_ztime{};

	struct KEYMAP {
		int keycode;
		void (*proc)(MyWindow&);
	};

	
	std::vector < KEYMAP > keys_mapped;

	static void onKey(GLFWwindow* window, int key, int scancode, int action, int mods)
	{
		MyWindow* obj = static_cast<MyWindow*>(glfwGetWindowUserPointer(window));
		obj->onKey(key, scancode, action, mods);
	}

	static void onFramebufferResize(GLFWwindow* window, int width, int height)
	{
		MyWindow* obj = static_cast<MyWindow*>(glfwGetWindowUserPointer(window));
		obj->onFramebufferResize(width, height);
	}

	static void onCursorPos(GLFWwindow* window, double xpos, double ypos)
	{
		MyWindow* obj = static_cast<MyWindow*>(glfwGetWindowUserPointer(window));
		obj->onCursorPos(xpos, ypos);
	}

	static void onMouseButton(GLFWwindow* window, int button, int action, int mods)
	{
		MyWindow* obj = static_cast<MyWindow*>(glfwGetWindowUserPointer(window));
		obj->onMouseButton(button, action, mods);
	}

	static void OnScroll(GLFWwindow* window, double xoffset, double yoffset)
	{
		MyWindow* obj = static_cast<MyWindow*>(glfwGetWindowUserPointer(window));
		obj->OnScroll(xoffset, yoffset);
	}


};

#endif