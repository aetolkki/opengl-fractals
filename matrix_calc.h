#ifndef MATRIX_CALC_H
#define MATRIX_CALC_H

#include <iostream>
#include <cmath>

void SetMVP(float aspect, float (&mvp)[16], float distance, float fov, float (&CameraPhase)[2], float yaw, float pitch);
void SetRotaZ(float time, float (&zrota)[16]);
void SetRotaX(float time, float (&xrota)[16]);
void SetRotaY(float time, float (&yrota)[16]);
void SetProjection(float aspect, float (&fovNearFar)[3], float (&projection)[16]);
void SetView(float (&eye)[3], float (&target)[3], float (&up)[3], float (&view)[16]);
void Normalize(float (&vec)[3]);
void Cross(float (&vec1)[3], float (&vec2)[3], float (&result)[3]);
void Multiply(float (&mat1)[16], float (&mat2)[16], float (&result)[16]);

#endif