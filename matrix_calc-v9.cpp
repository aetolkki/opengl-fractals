#define _USE_MATH_DEFINES

#include <iostream>
#include <cmath>

#include "matrix_calc.h"

void SetMVP(float aspect, float (&mvp)[16], float distance, float fov, float (&CameraPhase)[2], float yaw, float pitch)
{
	float fovNearFar[3]{fov, 0.1f, 100.0f}; 

	float CameraPos[3]{-CameraPhase[0], -CameraPhase[1], distance};
	float CameraFront[3]{(float)cos(yaw)*(float)cos(pitch), (float)sin(pitch), (float)sin(yaw)*(float)cos(pitch)};
	Normalize(CameraFront);
	float target[3]{CameraPos[0]+CameraFront[0], CameraPos[1]+CameraFront[1], CameraPos[2]+CameraFront[2]};
	float up[3]{0.0f, 1.0f, 0.0f};

	float projection[16]{};
	SetProjection(aspect, fovNearFar, projection);

	float view[16]{};
	SetView(CameraPos, target, up, view);	

	Multiply(projection, view, mvp);
}

void SetRotaZ(float time, float (&zrota)[16])
{
	zrota[0] = cos(time);
	zrota[1] = -sin(time);
	zrota[4] = sin(time);
	zrota[5] = cos(time);
	zrota[10] = 1.0f;
	zrota[15] = 1.0f;
}

void SetRotaX(float time, float (&xrota)[16])
{
	xrota[0] = 1.0f;
	xrota[5] = 0.995; //cos(time);
	xrota[6] = -0.0998; //-sin(time);
	xrota[9] = 0.0998; //sin(time);
	xrota[10] = 0.995; //cos(time);
	xrota[15] = 1.0f;
}

void SetRotaY(float time, float (&yrota)[16])
{
	yrota[0] = cos(time);
	yrota[2] = sin(time);
	yrota[5] = 1.0f;
	yrota[8] = -sin(time);
	yrota[10] = cos(time);
	yrota[15] = 1.0f;
}

void SetProjection(float aspect, float (&fovNearFar)[3], float (&projection)[16])
{
	float t{(float)tan((fovNearFar[0]*(float)M_PI/180.0f) / 2.0f) * fovNearFar[1]};
	float b{-t};
	float r{t*aspect};
	float l{b*aspect};

	projection[0] = 2.0f*fovNearFar[1] / (r-l);
	projection[5] = 2.0f*fovNearFar[1] / (t-b);
	projection[8] = (r+l) / (r-l);
	projection[9] = (t+b) / (t-b);
	projection[10] = (fovNearFar[2] +fovNearFar[1]) / (fovNearFar[1]-fovNearFar[2]);
	projection[11] = -1.0f; //What is this?
	projection[14] = 2.0f*fovNearFar[2]*fovNearFar[1]/(fovNearFar[1]-fovNearFar[2]);
	projection[15] = 1.0f;
}

void SetView(float (&eye)[3], float (&target)[3], float (&up)[3], float (&view)[16])
{
	float zax[3]{eye[0]-target[0], eye[1]-target[1], eye[2]-target[2]};
	Normalize(zax);
	float xax[3]{};
	Cross(up, zax, xax);
	Normalize(xax);
	float yax[3]{};
	Cross(zax, xax, yax);

	float x{-xax[0]*eye[0]+xax[1]*eye[1]+xax[2]*eye[2]};
	float y{-yax[0]*eye[0]+yax[1]*eye[1]+yax[2]*eye[2]};
	float z{-zax[0]*eye[0]+zax[1]*eye[1]+zax[2]*eye[2]};

	view[0] = xax[0];
	view[1] = yax[0];
	view[2] = zax[0];

	view[4] = xax[1];
	view[5] = yax[1];
	view[6] = zax[1];

	view[8] = xax[2];
	view[9] = yax[2];
	view[10] = zax[2];

	view[12] = x;
	view[13] = y;
	view[14] = z;
	view[15] = 1.0f;
}

void Normalize(float (&vec)[3])
{
	float mag{(float)sqrt(vec[0]*vec[0]+vec[1]*vec[1]+vec[2]*vec[2])};
	vec[0] /= mag;
	vec[1] /= mag;
	vec[2] /= mag;	
}

void Cross(float (&vec1)[3], float (&vec2)[3], float (&result)[3])
{
	result[0] = vec1[1]*vec2[2]-vec2[1]*vec1[2];
	result[1] = -(vec1[0]*vec2[2]-vec1[2]*vec2[0]);
	result[2] = vec1[0]*vec2[1]-vec2[0]*vec1[1];
}

void Multiply(float (&mat1)[16], float (&mat2)[16], float (&result)[16])
{
	result[0]=mat1[0]*mat2[0] + mat1[1]*mat2[4] + mat1[2]*mat2[8] + mat1[3]*mat2[12];
	result[1]=mat1[0]*mat2[1] + mat1[1]*mat2[5] + mat1[2]*mat2[9] + mat1[3]*mat2[13];
	result[2]=mat1[0]*mat2[2] + mat1[1]*mat2[6] + mat1[2]*mat2[10] + mat1[3]*mat2[14];
	result[3]=mat1[0]*mat2[3] + mat1[1]*mat2[7] + mat1[2]*mat2[11] + mat1[3]*mat2[15];

	result[4]=mat1[4]*mat2[0] + mat1[5]*mat2[4] + mat1[6]*mat2[8] + mat1[7]*mat2[12];
	result[5]=mat1[4]*mat2[1] + mat1[5]*mat2[5] + mat1[6]*mat2[9] + mat1[7]*mat2[13];
	result[6]=mat1[4]*mat2[2] + mat1[5]*mat2[6] + mat1[6]*mat2[10] + mat1[7]*mat2[14];
	result[7]=mat1[4]*mat2[3] + mat1[5]*mat2[7] + mat1[6]*mat2[11] + mat1[7]*mat2[15];

	result[8]=mat1[8]*mat2[0] + mat1[9]*mat2[4] + mat1[10]*mat2[8] + mat1[11]*mat2[12];
	result[9]=mat1[8]*mat2[1] + mat1[9]*mat2[5] + mat1[10]*mat2[9] + mat1[11]*mat2[13];
	result[10]=mat1[8]*mat2[2] + mat1[9]*mat2[6] + mat1[10]*mat2[10] + mat1[11]*mat2[14];
	result[11]=mat1[8]*mat2[3] + mat1[9]*mat2[7] + mat1[10]*mat2[11] + mat1[11]*mat2[15];

	result[12]=mat1[12]*mat2[0] + mat1[13]*mat2[4] + mat1[14]*mat2[8] + mat1[15]*mat2[12];
	result[13]=mat1[12]*mat2[1] + mat1[13]*mat2[5] + mat1[14]*mat2[9] + mat1[15]*mat2[13];
	result[14]=mat1[12]*mat2[2] + mat1[13]*mat2[6] + mat1[14]*mat2[10] + mat1[15]*mat2[14];
	result[15]=mat1[12]*mat2[3] + mat1[13]*mat2[7] + mat1[14]*mat2[11] + mat1[15]*mat2[15];
}