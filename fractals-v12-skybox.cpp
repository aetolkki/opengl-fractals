#define _USE_MATH_DEFINES
#define STB_IMAGE_IMPLEMENTATION

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include "src/stb_image.h"

#include "shaders.h"
#include "matrix_calc.h"
#include "windowcontrols.h"

#include <iostream>
#include <cmath>
#include <cstdlib> // for std::rand() and std::srand() 

//1 quadrant = 18 points (2 triangles = 6 vertices = 6*3 x,y,z points)
//Static memory allocation, reference to grid array:
//void setGrid(float (&grid)[gridSize*18]);
//Dynamic memory allocation, pointer to grid array:
void setGrid(float *&grid, int gridwidth, int gridheight);
void set_color_palette(float (&palette)[21*3]);
float set_mutation(float randangle, float (&mutas)[2]);

int main()
{
	const int gridwidth{480};
	const int gridheight{270};
	const long int gridSize{gridwidth*gridheight}; // +1 

	//{x, y, zoom, aspect, cameradistance}
	MyWindow ikkuna{0.0f, 0.0f, 1.0f, 800.0f/600.0f, 0.5f};

	// build and compile our shader program
	Shader ourShader("fractals-v11.vs", "fractals-v11.fs");
	Shader background("bg-skybox.vs", "bg-skybox.fs"); 
	
	//Background image texture
	float skyboxVertices[] = {
		// positions          
		-1.0f, -1.0f, -1.0f,
		 1.0f, -1.0f, -1.0f,
		 -1.0f,  1.0f, -1.0f,
		 -1.0f,  1.0f, -1.0f,
		 1.0f, -1.0f, -1.0f,
		 1.0f,  1.0f, -1.0f,
		
		-1.0f, -1.0f,  1.0f,
		-1.0f, -1.0f, -1.0f,
		-1.0f,  1.0f,  1.0f,
		-1.0f,  1.0f, 1.0f,
		-1.0f, -1.0f, -1.0f,
		-1.0f, 1.0f,  -1.0f,

		 1.0f, -1.0f, -1.0f,
		 1.0f, -1.0f,  1.0f,
		 1.0f,  1.0f, -1.0f,
		 1.0f,  1.0f, -1.0f,
		 1.0f, -1.0f,  1.0f,
		 1.0f,  1.0f,  1.0f,
		 
		1.0f, 	-1.0f, 1.0f,
		-1.0f,  -1.0f, 1.0f,
		1.0f,  	1.0f,  1.0f,
		1.0f,  	1.0f,  1.0f,
		-1.0f, 	-1.0f, 1.0f,
		-1.0f, 	1.0f,  1.0f,

		-1.0f,  1.0f, -1.0f,
		 1.0f,  1.0f, -1.0f,
		-1.0f,  1.0f,  1.0f,
		-1.0f,  1.0f,  1.0f,
		1.0f,  1.0f,  -1.0f,
		1.0f,  1.0f,   1.0f,

		-1.0f, -1.0f, 1.0f,
		1.0f, -1.0f,  1.0f,
		-1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f, -1.0f,
		1.0f, -1.0f,  1.0f,
		 1.0f, -1.0f,  -1.0f
	};

	//Dynamic memory allocation
	float *vertices{new float[gridSize*18]{}};
	setGrid(vertices, gridwidth, gridheight);

	unsigned int VBO, VAO, skyboxvao, skyboxvbo;
	glGenVertexArrays(1, &VAO);
	glGenBuffers(1, &VBO);
	// bind the Vertex Array Object first, then bind and set vertex buffer(s), and then configure vertex attributes(s).
	glBindVertexArray(VAO);

	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, gridSize*18*sizeof(float), vertices, GL_STATIC_DRAW);
	// position attribute
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(0);
	//Delete pointer after position array has been sent to shaders
	delete[] vertices;

	//Texture init here
	glGenVertexArrays(1, &skyboxvao);
	glGenBuffers(1, &skyboxvbo);
	glBindVertexArray(skyboxvao);
	glBindBuffer(GL_ARRAY_BUFFER, skyboxvbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(skyboxVertices), skyboxVertices, GL_STATIC_DRAW);
	// position attribute
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(0); //layout (location = 0)
	
	// load and create a texture 
	// -------------------------
	unsigned int textureID;
	glGenTextures(1, &textureID);    
	glBindTexture(GL_TEXTURE_CUBE_MAP, textureID); 
	
	////////////////////////////////////////////////////
	int sbwidth, sbheight, nrChannels;
	unsigned char *data = stbi_load("mandelbrot-pulse.jpg", &sbwidth, &sbheight, &nrChannels, 0);
	for (unsigned int i = 0; i < 6; i++)
	{	
		if (data)
		{
    		glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGB, sbwidth, sbheight, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
		}
		else
		{
    		std::cout << "Cubemap texture failed to load at path: " << i;
		}
	}
	stbi_image_free(data);
	
	// set the texture wrapping parameters
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_REPEAT);	
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_REPEAT);
	// set texture filtering parameters
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	//////////////////////////////////////////////////////

	//Set Uniforms
	float time{0.0f};
	float dt{0.0f};
	float frametime{};
	float now{0.0f};
	float mvp[16]{};
	float zrota[16]{};
	float xrota[16]{};
	float yrota[16]{};
	
	float coords[2]{};
	float CameraPhase[2]{};
	float mutas[2]{};
	float mutasboost[2]{};
	//mutas[0]=-0.38f;
	//mutas[0]=0.27f;
	float randangle{};
	float zoomboost{1.0f};

	float palette[21*3]{};
	set_color_palette(palette);
	
	// render loop
	while (!glfwWindowShouldClose(ikkuna.getWindow()))
	{ 
		// render
		glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		//glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC1_COLOR);
		glEnable( GL_BLEND );
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glEnable(GL_DEPTH_TEST);
		glFrontFace(GL_CCW); //GL_CW; clockwise
		glEnable(GL_CULL_FACE);

		ourShader.use();
		//render grid
		glBindVertexArray(VAO);
		glDrawArrays(GL_TRIANGLES, 0, gridSize*6);
		glBindVertexArray(0);

		//render skybox
		glDepthFunc(GL_LEQUAL);
		glDepthMask(GL_FALSE);
		//glDisable(GL_DEPTH_TEST);
		background.use();
		glBindVertexArray(skyboxvao);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_CUBE_MAP, textureID);
		glDrawArrays(GL_TRIANGLES, 0, 6*6);
		glBindVertexArray(0);
		glDepthFunc(GL_LESS); // set depth function back to default
		glDepthMask(GL_TRUE);
		
		now = 0.1*glfwGetTime();
		if (now - time > 0.0) 
		{
			dt = now - time;
			time = now;
		}
		//Zero time after 2*PI
		//if (time >= 6.2832) 
		//{
		//	glfwSetTime(0.0);
		//	time = 0;
		//	zoomboost = 0.1*ikkuna.getZoom();
		//}

		float pulse{};
		//pulse = 1.5f*fmod(time,0.1f);
		//pulse = 0.15f*sin(time);
		pulse = 0.1f*(exp(-pow(fmod(time,0.1f)-0.01,2)/0.0001) + exp(-pow(fmod(time,0.1f)-0.04,2)/0.001));

		//zoomboost = (1.0f-fmod(time,0.1f)) * ikkuna.getZoom();
		zoomboost = ikkuna.getZoom();
		if(!ikkuna.getMutaRandom())
		{	
			mutasboost[0] = pulse + mutas[0];
			mutasboost[1] = pulse + mutas[1];
		} else { mutasboost[0]=mutas[0]; mutasboost[1]=mutas[1];}

		frametime += dt;

		ikkuna.setDt(dt);
		zoomboost *= 1.01f;

		//SetRotaZ(time, zrota);
		SetRotaZ(ikkuna.getZtime(), zrota);
		ikkuna.updateZrota(zrota);
		SetRotaX(time, xrota);
		SetRotaY(time, yrota);
		ikkuna.updateYrota(yrota);

		ikkuna.processKeys();

		coords[0] = ikkuna.getX(); // + dragX;
		coords[1] = ikkuna.getY(); // + dragY;

		CameraPhase[0] = ikkuna.getCameraphase()[0];
		CameraPhase[1] = ikkuna.getCameraphase()[1];
	
		SetMVP(ikkuna.getAspect(), mvp, ikkuna.getDistance(), ikkuna.getFov(), CameraPhase, ikkuna.getYaw(), ikkuna.getPitch());


		if (frametime > 0.001f * -log(zoomboost) && ikkuna.getMutaRandom()) { randangle = set_mutation(randangle, mutas); frametime = 0.0f; }
		if (ikkuna.getMutaReset()) {mutas[0]=0.0f; mutas[1]=0.0f; ikkuna.setMutaReset();}
		if (ikkuna.getColorSet()) {set_color_palette(palette); ikkuna.setColorset();}

		//send uniforms
		ourShader.use();
		ourShader.setInt("modeUniform", ikkuna.getMode());
		ourShader.setFloat("timeUniform", time);
		ourShader.setFloat("aspectUniform", ikkuna.getAspect());
		ourShader.setMatrix("zrotaUniform", zrota);
		ourShader.setMatrix("xrotaUniform", xrota);
		ourShader.setMatrix("yrotaUniform", yrota);
		ourShader.setMatrix("mvpUniform", mvp);

		ourShader.setVec2("coord", coords);
		ourShader.setFloat("zoom", zoomboost);

		ourShader.setVec3Array("colors", palette, 21);

		ourShader.setVec2("mutaUniform", mutasboost);

		background.use();
		background.setInt("skybox", 0);
		background.setMatrix("mvpUniform", mvp);
		background.setMatrix("zrotaUniform", zrota);


		// glfw: swap buffers and poll IO events (keys pressed/released, mouse moved etc.)
		ikkuna.swapBuffers();
	}

	// optional: de-allocate all resources once they've outlived their purpose:
	glDeleteVertexArrays(1, &VAO);
	glDeleteBuffers(1, &VBO);
	glDeleteVertexArrays(1, &skyboxvao);
	glDeleteBuffers(1, &skyboxvbo);

	return 0;
}

//Static memory allocation, reference to grid array:
//void setGrid(float (&grid)[gridSize*18])
//Dynamic memory allocation, pointer to grid array:
void setGrid(float *&grid, int gridwidth, int gridheight)
{
	float gridAspect = (float) gridwidth / (float) gridheight;
	float gridX{-0.6f*gridAspect};
	//float gridY{0.6f};
	float gridY{1.2f};
	float shift{1.2f / (float) gridheight};

	int index{0};

	for (int countY{0}; countY<gridheight; ++countY) 
	{
		for (int countX{0}; countX<gridwidth; ++countX) 
		{
			//First triangle------------------
			grid[index] = gridX;
			grid[index+1] = gridY-shift;
			grid[index+2] = 0.0f;

			grid[index+3] = gridX+shift;
			grid[index+4] = gridY-shift;
			grid[index+5] = 0.0f;

			grid[index+6] = gridX;
			grid[index+7] = gridY;
			grid[index+8] = 0.0f;

			//Second triangle------------------
			grid[index+9] = gridX;
			grid[index+10] = gridY;
			grid[index+11] = 0.0f;

			grid[index+12] = gridX+shift; 
			grid[index+13] = gridY-shift;
			grid[index+14] = 0.0f;

			grid[index+15] = gridX+shift;
			grid[index+16] = gridY;
			grid[index+17] = 0.0f;

			gridX += shift;
			index += 18; //if (gridX<0.5f*gridAspect) index += 18;			
		}
		gridY -= shift;
		gridX = -0.6f*gridAspect;
	}
}

void set_color_palette(float (&palette)[21*3])
{
	std::srand(time(0));
	std::rand();

	for (int col{0}; col<21*3; ++col)
	{
		palette[col] = (float)std::rand() / (float)RAND_MAX;
	}
}


float set_mutation(float randangle, float (&mutas)[2])
{
	float step{0.001f};
	randangle += (float)M_PI*(0.5*std::rand()/RAND_MAX - 0.25); 
	if (mutas[0] < -0.5f)
	{
		mutas[0] = -0.5f;
		randangle = (float)M_PI - randangle;
	}	
	else if (mutas[0] > 0.5f)
	{
		mutas[0] = 0.5f;
		randangle = (float)M_PI - randangle;
	}
	else if (mutas[1] < -0.5f)
	{
		mutas[1] = -0.5f;
		randangle *= -1.0f;
	}
	else if (mutas[1] > 0.5f)
	{
		mutas[1] = 0.5f;
		randangle *= -1.0f;
	}
	mutas[0] += step*cos(randangle);
	mutas[1] += step*sin(randangle);

	return randangle;
}

//g++ -o fractals-bin fractals-v9.cpp matrix_calc-v9.cpp windowcontrols.cpp src/glad.c -lglfw -ldl -std=c++17 -Wall
//usr/include/glad
//usr/include/GLFW

//for debugging:
//g++ -g fractals-v9.cpp matrix_calc-v9.cpp src/glad.c -o testaus -lglfw -ldl -std=c++17 -Wall
//gdb ./testaus


//smooth colors -checked
//other newton modes -checked