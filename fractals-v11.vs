#version 330 core
layout (location = 0) in vec3 aPos;

#define LOG2XLN10    0.69314718
#define LOG2XLN10X2  1.38629436

out vec4 ourColor;

uniform float timeUniform;
uniform int modeUniform;

uniform vec2 coord;
uniform float zoom;

uniform mat4 mvpUniform;
uniform mat4 xrotaUniform;
uniform mat4 yrotaUniform;
uniform mat4 zrotaUniform;

uniform vec3 colors[21];

uniform vec2 mutaUniform;

const float r4[12] = float[12](-1.76929, 0.88465, 0.88465, 0.0, 0.0, 0.0, 0.0, 0.58974, -0.58974, 0.0, 0.0, 0.0);
const float r5[12] = float[12](1.0, -0.5, -0.5, 0.0, 0.0, 0.0, 0.0, 0.8660254, -0.8660254, 0.0, 0.0, 0.0);
const float r6[12] = float[12](-1.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, -1.0, 1.0, 0.0, 0.0);
const float r7[12] = float[12](0.8518, -1.17398, 0.58699, 0.58699, -0.4259,-0.4259, 0.0, 0.0, 1.0167, -1.0167, 0.73768, -0.73768);
			
float r[12];

const float tolerance = 0.001;

void newiter(in vec2 zin, out vec2 zout)
{
    float a = 1.0 + 3.0*mutaUniform.x;
    float zinx_zinx = zin.x*zin.x;
    float ziny_ziny = zin.y*zin.y;
    float zinx_ziny = zin.x*zin.y;

    float h1 = zinx_zinx*zin.x-3.0*zin.x*ziny_ziny-2.0*zin.x+2.0;
    float h2 = 3.0*zinx_zinx-3.0*ziny_ziny-2.0;
    float h3 = 3.0*zinx_zinx*zin.y-ziny_ziny*zin.y-2.0*zin.y;

    float divisor = (h2*h2+36.0*zinx_zinx*ziny_ziny);
    if (divisor == 0.0) divisor = tolerance;
    zout.x = a*(h1*h2+6.0*zinx_ziny*h3)/divisor;
    zout.y = a*(h3*h2-6.0*zinx_ziny*h1)/divisor;
}

void newiter2(in vec2 zin, out vec2 zout) 
{
	if (modeUniform == 5) r = r5;
	else if (modeUniform == 6) r = r6;
	else if (modeUniform == 7) r = r7;
	vec2 muta = mutaUniform;

	float divisor = (zin.x-r[0])*(zin.x-r[0])+zin.y*zin.y;
	if (divisor == 0.0) divisor = tolerance;
	float tr = ((muta.x+0.5)*(zin.x-r[0]) + muta.y*zin.y)/divisor;
	float ti = (-(muta.x+0.5)*zin.y + (zin.x-r[0])*muta.y)/divisor;
		
	for (int l = 1; l<6; l++) 
	{
		if (r[l] != 0.0 || r[l+6] != 0.0) 
		{
			if (zin.x == r[l] && zin.y == r[l+6]) zin += vec2(tolerance, tolerance);
			tr += (zin.x-r[l])/((zin.x-r[l])*(zin.x-r[l])+(zin.y-r[l+6])*(zin.y-r[l+6]));
			ti += -(zin.y-r[l+6])/((zin.x-r[l])*(zin.x-r[l])+(zin.y-r[l+6])*(zin.y-r[l+6]));
	  	} else break;
	}

	divisor = (tr*tr + ti*ti); 
	if (divisor == 0.0) divisor = tolerance;
	zout.x = tr/divisor;
	zout.y = -ti/divisor;
}


void setColor(in int j, in float colorVal, out vec3 color)
{	
	colorVal += 0.1 * float(j);

	float colorphase = colorVal + 4.0*mod(0.1592*timeUniform, 0.25);
	int colorindex = int(20.0*colorphase);
	while (colorindex > 20) 
	{
		if (colorindex > 20) colorindex -= 20;
	}

	float phase = clamp(20.0*colorphase-floor(20.0*colorphase), 0.0, 1.0);
	if (colorindex==20 || colorindex==0) color = mix(colors[20], colors[1], phase);
	else color = mix(colors[colorindex], colors[colorindex+1], phase);
}

void newton(in int mode, in vec2 z, out vec4 rgb)
{
	float cor;
	float s = 0.0;
	float colorVal = 1.0;

	float olddiff = 100.0;
	float newdiff = 100.0;
	float tempdiff = 0.0;
	float newtempdiff = 0.0;
	int rootnumber = 6;
	if (modeUniform == 4) r = r4;
	else if (modeUniform == 5) r = r5;
	else if (modeUniform == 6) r = r6;
	else if (modeUniform == 7) r = r7;

	vec2 xy;
	vec2 znew;
	vec3 color = vec3(0.0,0.0,0.0);
	rgb = vec4(0.0,0.0,0.0,0.0);

	int j;

	for (int i=0; i<100; i++)
	{
		cor = float(i);
		if (modeUniform == 4) newiter(z, znew);
		else newiter2(z, znew);
		xy = z - znew;

		bool colorSet = false;

		for (j=0; j<6; j++)
		{
			tempdiff = length(z-vec2(r[j], r[j+6]));
			newtempdiff = length(xy-vec2(r[j], r[j+6]));

			if (tempdiff < olddiff) olddiff = tempdiff;
		
			if (newtempdiff < newdiff) 
			{
				newdiff = newtempdiff;
				//-6.9078 = "lntolerance"
				s = (-6.9078 - log(olddiff)) / (log(newdiff) - log(olddiff));
			}

			//Assign color for different roots (tolerance = 0.001)
			if (newdiff < tolerance) 	
			{
				colorVal = (cor+s+1.0) / 100.0;
				setColor(j, colorVal, color);
				colorSet = true; 
				break;
			}
		}

		z = xy;
		if (i==99) setColor(0, 1.0, color);
		rgb.xyz = color;
		rgb.w = clamp(colorVal,0.0,0.5); // + 0.01*float(j);

		if (i == 99 || colorSet) break;
	}
}

void mandelbrot(in int mode, in vec2 zin, out vec4 rgb)
{
	vec2 z;
	vec2 xy;
	vec2 norm;
	float derivz = 0.0;
	vec3 mandelcolor;
	float distestim;
	vec2 add;
	float i, h;

	norm = zin; 
	vec2 juliamuta = vec2(0.5 * (1 - cos(0.1*timeUniform)) * cos(0.1*timeUniform) + 0.25, 0.5 * (1 - cos(0.1*timeUniform)) * sin(0.1*timeUniform));

	if (mode == 1) { z = norm + mutaUniform; add = norm; } // Mandel
	//else if (mode == 2) { z = norm + mutaUniform; add = mutaUniform; } // Julia
	else if (mode == 2) { z = norm; add = juliamuta + mutaUniform; } // Julia
	else if (mode == 3) { z = abs(norm + mutaUniform); add.x = norm.x; add.y = -norm.y; } //Burning Ship

	for (i = 0.0; i < 150.0; i++) {
		xy.x = (z.x * z.x - z.y * z.y) + add.x; 
		xy.y = (2.0 * z.x * z.y) + add.y;

		derivz = length(2.0*z*derivz+1.0);
		if(dot(xy,xy) > 4.0) { 
			float cor = i + 1.0 - log(log(dot(xy,xy))/LOG2XLN10X2) / LOG2XLN10;
			distestim = 2.0*length(z)*log(length(z))/derivz;
			if(distestim > 0.005 || distestim < 0.0) distestim = 0.005;	
			if(mode == 3) { h = i/150.0; setColor(0, i/150.0, mandelcolor); }
			else setColor(0, distestim/0.005, mandelcolor);		
			break;
		} 
		if (mode < 3) z = xy; //Mandel, Julia
		else z = abs(xy); //BurningShip

		if (i == 149.0) 
		{
			setColor(0, i/150.0, mandelcolor);
			distestim = 0.0;
			h = i/150.0;
		}	
	}
	rgb = vec4(mandelcolor, distestim);
	//rgb = vec4(mandelcolor, h); //numsteps based color scale
}

void valikko(in int mode, in vec2 zin, out vec4 rgb)
{
	switch(mode)
	{
		case 1: 
			mandelbrot(mode, zin, rgb);
			rgb.w = pow(100.0*rgb.w, 1.0);
			//rgb.w = pow(100.0*rgb.w, 0.9);
			break;
		case 2:	//julia
			mandelbrot(mode, zin, rgb);
			rgb.w = 0.5*pow(100.0*rgb.w, 1.0);
			break;
		case 3: //burningship
			mandelbrot(mode, zin, rgb);
			rgb.w = pow(100.0*rgb.w, 0.9);
			break;	
		default: //Newtons
			newton(mode, zin, rgb);
			//rgb.w = -0.2*pow(rgb.w, 0.8);
			rgb.w = -0.5*rgb.w;
			break;
	}
}

void main()
{
	float alpha;
	float levelshift;
	if (modeUniform<4) levelshift = -0.15;
	else levelshift = 0.0;
	vec4 newPosition = vec4(aPos, 1.0);
	vec4 rgb;
	vec2 z = zoom*newPosition.xy + (zrotaUniform * vec4(coord,0.0,0.0)).xy;
	//vec2 z = zoom*newPosition.xy + coord; //no z rotation
	
	mat4 camera = transpose(zrotaUniform); //z rotation
	z = (camera * vec4(z,0.0,0.0)).xy;

	valikko(modeUniform, z, rgb);
	//Flip coordinates, xz= gridplane y=height
	newPosition.z = -newPosition.y + 1.0;
	newPosition.y = rgb.w + levelshift;

	alpha = 0.9;

	gl_Position = mvpUniform * newPosition;
	ourColor = vec4(rgb.xyz, alpha);
}
