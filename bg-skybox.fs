#version 330 core
out vec4 FragColor;

in vec3 TexCoords;
uniform mat4 mvpUniform;
uniform samplerCube skybox;

void main()
{    
    FragColor = mvpUniform * texture(skybox, TexCoords);
}
